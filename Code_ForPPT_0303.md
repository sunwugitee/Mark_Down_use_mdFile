#中期检查PPT的代码。   
write by SunWu


##集成测试程序
```
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#define LOS_OK 0
#define LOS_NOK -1
int main ()
{
    int ret;
    ssize_t len;
    int fd = -1;
    //get main pid
    int mainpid = getpid();
    char plimitsA[128] = "/proc/plimits/plimitsA";
    char plimitsAPids[128] = "/proc/plimits/plimitsA/pids.max";
    char plimitsAMemoryLimit[128] = "/proc/plimits/plimitsA/memory.limit";
    char plimitsAMemoryUsage[128] = "/proc/plimits/plimitsA/memory.usage";
    char plimitsAProcs[128] = "/proc/plimits/plimitsA/plimits.procs";
    char plimitsAAdd[128] = "/proc/plimits/plimitsA/plimits.limiter_add";
    char plimitsADelete[128] = "/proc/plimits/plimitsA/plimits.limiter_delete";
    char plimitsMem[128] = "/proc/plimits/memory.usage";
    char plimitsPid[128] = "/proc/plimits/plimits.procs";
    char *mem = NULL;
    char writeBuf[128];
    char readBuf[128];

    /* 查看根plimits组内进程 */
    memset(readBuf, 0, sizeof(readBuf));
    fd = open(plimitsPid, O_RDONLY);
    len = read(fd, readBuf, sizeof(readBuf));
    if (len != strlen(readBuf)) {
        printf("read file failed.\n");
        return LOS_NOK;
    }
    close(fd);
    printf("/proc/plimits组内进程：%s\n", readBuf);

    /* 查看根plimits组内内存使用 */
    memset(readBuf, 0, sizeof(readBuf));
    fd = open(plimitsMem, O_RDONLY);
    len = read(fd, readBuf, sizeof(readBuf));
    if (len != strlen(readBuf)) {
        printf("read file failed.\n");
        return LOS_NOK;
    }
    close(fd);
    printf("/proc/plimits组内已使用内存：%s\n", readBuf);


    /* 创建plimitsA “/proc/plimits/plimitsA” */
    ret = mkdir(plimitsA, 0777);
    if (ret != LOS_OK) {
        printf("mkdir failed.\n");
        return LOS_NOK;
    }

    /* 设置plimitsA组允许挂载进程个数 */
    memset(writeBuf, 0, sizeof(writeBuf));
    sprintf(writeBuf, "%d", 3);
    fd = open(plimitsAPids, O_WRONLY);
    len = write(fd, writeBuf, strlen(writeBuf));
    if (len != strlen(writeBuf)) {
        printf("write file failed.\n");
        return LOS_NOK;
    }
    close(fd);

    /* 挂载进程至plimitsA组 */
    memset(writeBuf, 0, sizeof(writeBuf));
    sprintf(writeBuf, "%d", mainpid);
    fd = open(plimitsAProcs, O_WRONLY);
    len = write(fd, writeBuf, strlen(writeBuf));
    if (len != strlen(writeBuf)) {
        printf("write file failed.\n");
        return LOS_NOK;
    }
    close(fd);

    /* 设置plimitsA组内分配内存限额 */
    memset(writeBuf, 0, sizeof(writeBuf));
    //limit memory
    sprintf(writeBuf, "%d", (1024*1024*3));
    fd = open(plimitsAMemoryLimit, O_WRONLY);
    len = write(fd, writeBuf, strlen(writeBuf));
    if (len != strlen(writeBuf)) {
        printf("write file failed.\n");
        return LOS_NOK;
    }
    close(fd);

    /* 查看plimitsA组内允许使用的最大内存 */
    memset(readBuf, 0, sizeof(readBuf));
    fd = open(plimitsAMemoryLimit, O_RDONLY);
    len = read(fd, readBuf, sizeof(readBuf));
    if (len != strlen(readBuf)) {
        printf("read file failed.\n");
        return LOS_NOK;
    }
    close(fd);
    printf("/proc/plimits/plimitsA组允许使用的最大内存：%s\n", readBuf);

    /* 查看plimitsA组内挂载的进程 */
    memset(readBuf, 0, sizeof(readBuf));
    fd = open(plimitsAProcs, O_RDONLY);
    len = read(fd, readBuf, sizeof(readBuf));
    if (len != strlen(readBuf)) {
        printf("read file failed.\n");
        return LOS_NOK;
    }
    close(fd);
    printf("/proc/plimits/plimitsA组内挂载的进程：%s\n", readBuf);

    /* 查看plimitsA组内存的使用情况 */
    mem = (char*)malloc(1024*1024);
    memset(mem, 0, 1024);
    memset(readBuf, 0, sizeof(readBuf));
    fd = open(plimitsAMemoryUsage, O_RDONLY);
    len = read(fd, readBuf, sizeof(readBuf));
    if (len != strlen(readBuf)) {
        printf("read file failed.\n");
        return LOS_NOK;
    }
    close(fd);
    printf("/proc/plimits/plimitsA组已使用内存：%s\n", readBuf);

    /* 删除plimitsA组内memory限制器 */
    memset(writeBuf, 0, sizeof(writeBuf));
    sprintf(writeBuf, "%s", "memory");
    fd = open(plimitsADelete, O_WRONLY);
    len = write(fd, writeBuf, strlen(writeBuf));
    if (len != strlen(writeBuf)) {
        printf("write file failed.\n");
        return LOS_NOK;
    }
    close(fd);

    /* 增加plimitsA组内memory限制器 */
    memset(writeBuf, 0, sizeof(writeBuf));
    sprintf(writeBuf, "%s", "memory");
    fd = open(plimitsAAdd, O_WRONLY);
    len = write(fd, writeBuf, strlen(writeBuf));
    if (len != strlen(writeBuf)) {
        printf("write file failed.\n");
        return LOS_NOK;
    }
    close(fd);

    /* 删除plimitsA组,首先删除memory、pids、sched限制器 */
    memset(writeBuf, 0, sizeof(writeBuf));
    sprintf(writeBuf, "%s", "memory");
    fd = open(plimitsADelete, O_WRONLY);
    len = write(fd, writeBuf, strlen(writeBuf));
    if (len != strlen(writeBuf)) {
        printf("write file failed.\n");
        return LOS_NOK;
    }
    memset(writeBuf, 0, sizeof(writeBuf));
    sprintf(writeBuf, "%s", "pids");
    fd = open(plimitsADelete, O_WRONLY);
    len = write(fd, writeBuf, strlen(writeBuf));

    memset(writeBuf, 0, sizeof(writeBuf));
    sprintf(writeBuf, "%s", "sched");
    fd = open(plimitsADelete, O_WRONLY);
    len = write(fd, writeBuf, strlen(writeBuf));
    close(fd);
    ret = rmdir(plimitsA);
    if (ret != LOS_OK) {
        printf("rmdir failed.\n");
        return LOS_NOK;
    }

    return 0;
}
```

编译运行得到的结果为：
/proc/plimits组内进程：
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15

/proc/plimits组内已使用内存：28016640

/proc/plimits/plimitsA组允许使用的最大内存：3145728

/proc/plimits/plimitsA组内挂载的进程：
15

/proc/plimits/plimitsA组已使用内存：4096


##容器隔离和容器配额的功能展示的APP程序
```
#include <cstdio>
#include <unistd.h>
#include <cstdlib>
#include <fcntl.h>
#include <vector>
#include <string>
#include "It_process_plimits.h"

static bool AssertFilesExisting(std::string path, std::vector<std::string> &file)
{
    for (auto iter = file.begin(); iter != file.end(); ++iter) {
        std::string fullPath = path + "/" + *iter;
        int ret = access(fullPath.c_str(), F_OK);
        ICUNIT_ASSERT_EQUAL(ret, 0, ret);
    }
    return true;
}

static bool AssertFilesNotExisting(std::string path, std::vector<std::string> &file)
{
    for (auto iter = file.begin(); iter != file.end(); ++iter) {
        std::string fullPath = path + "/" + *iter;
        int ret = access(fullPath.c_str(), F_OK);
        ICUNIT_ASSERT_NOT_EQUAL(ret, 0, ret);
    }
    return true;
}

static int LimiterFileWrite(std::string path, std::string buf)
{
    std::string fullPathR = path + "/" + "plimits.limiter_delete";
    int fd = open(fullPathR.c_str(), O_WRONLY);
    ICUNIT_ASSERT_NOT_EQUAL(fd, -1, fd);
    size_t rst = write(fd, buf.c_str(), (buf.length()+1));
    ICUNIT_ASSERT_NOT_EQUAL(rst, -1, rst);
    (void)close(fd);
    return rst;
}

static UINT32 Testcase(void)
{
    std::string plimitsTestPath = "/proc/plimits/test";
    std::string plimitsSubTestPath = "/proc/plimits/test/subtest";
    std::string deletePath = plimitsTestPath + "/" + "plimits.limiter_delete";
    std::string addPath = plimitsSubTestPath + "/" + "plimits.limiter_add";
    mode_t mode = S_IRWXU;
    int ret = mkdir(plimitsTestPath.c_str(), mode);
    ICUNIT_ASSERT_EQUAL(ret, 0, ret);

    std::vector<std::string> filePathR;
    filePathR.push_back("memory.failcnt");
    filePathR.push_back("memory.limit");
    filePathR.push_back("memory.usage");
    filePathR.push_back("memory.stat");

    std::string buf = "memory";
    ret = WriteFile(deletePath.c_str(), buf.c_str());
    ICUNIT_GOTO_NOT_EQUAL(ret, -1, ret, EXIT);
    ICUNIT_GOTO_EQUAL(AssertFilesNotExisting(plimitsTestPath, filePathR), true, 0, EXIT);

    ret = mkdir(plimitsSubTestPath.c_str(), mode);
    ICUNIT_GOTO_EQUAL(ret, 0, ret, EXIT);

    ret = WriteFile(addPath.c_str(), buf.c_str());
    ICUNIT_GOTO_NOT_EQUAL(ret, -1, ret, EXIT1);
    ICUNIT_GOTO_EQUAL(AssertFilesExisting(plimitsSubTestPath, filePathR), true, -1, EXIT);

    (void)RmdirTest(plimitsSubTestPath);
    (void)RmdirTest(plimitsTestPath);
    return 0;

EXIT:
    (void)RmdirTest(plimitsTestPath);
    return 0;

EXIT1:
    (void)RmdirTest(plimitsSubTestPath);
    (void)RmdirTest(plimitsTestPath);
    return 0;
}

void ItProcessPlimits026(void)
{
    TEST_ADD_CASE("IT_PROCESS_PLIMITS_026", Testcase, TEST_PROCESS, TEST_PLIMITS, TEST_LEVEL0, TEST_FUNCTION);
}
```
